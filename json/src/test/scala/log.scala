import kadai.log.LogWriter
import kadai.log.json.{ JsonLogging, JsonMessage }

/**
 * manual testing
 */
object log extends JsonLogging {
  import JsonLogging._
  def apply[A: JsonMessage.Qualified](a: A) = warn(a)

  override def error[A: LogWriter](msg: => A) = super.error(msg)
  override def warn[A: LogWriter](msg: => A) = super.warn(msg)
  override def info[A: LogWriter](msg: => A)= super.info(msg)
  override def withInfo[A: LogWriter, B](msg: => A)(f: => B) = super.withInfo(msg)(f)
  override def debug[A: LogWriter](msg: => A)= super.debug(msg)
  override def withDebug[A: LogWriter, B](msg: => A)(f: => B) = super.withDebug(msg)(f)
  override def trace[A: LogWriter](msg: => A)= super.trace(msg)
  override def withTrace[A: LogWriter, B](msg: => A)(f: => B) = super.withTrace(msg)(f)
}
