import sbt._, Keys._
import sbtrelease.ReleasePlugin._
import Dependencies._

object Settings {
  // "-language:_"
  //   Seq( "-Xlog-free-terms")

  val scalacFlags =  Seq(
    "-deprecation"
  , "-encoding", "UTF-8" // yes, this is 2 args
  , "-target:jvm-1.8"
  , "-feature"
  , "-language:existentials"
  , "-language:experimental.macros"
  , "-language:higherKinds"
  , "-language:implicitConversions"
  , "-unchecked"
  , "-Xfatal-warnings"
//  , "-Xlint" // commented out due to https://issues.scala-lang.org/browse/SI-8476
  , "-Yno-adapted-args"
  //, "-Ywarn-all"
  //, "-Yrangepos"
  , "-Ywarn-dead-code" // N.B. doesn't work well with the ??? hole
  , "-Ywarn-numeric-widen"
  , "-Ywarn-value-discard"     
  )

  lazy val standardSettings = 
    Defaults.coreDefaultSettings ++ 
    Release.customReleaseSettings ++ // sbt-release
    Seq[Def.Setting[_]] (
      organization := "io.atlassian"
    , licenses := Seq("Apache2" -> url("https://bitbucket.org/atlassian/kadai/raw/master/LICENSE"))
    , homepage := Some(url("https://bitbucket.org/atlassian/kadai"))
    , pomExtra := (
      <scm>
          <url>git@bitbucket.org:atlassian/kadai-log.git</url>
          <connection>scm:git:git@bitbucket.org:atlassian/kadai-log.git</connection>
          <developerConnection>scm:git:git@bitbucket.org:atlassian/kadai-log.git</developerConnection>
      </scm>
      <distributionManagement>
          <repository>
              <id>atlassian-public</id>
              <name>Atlassian Public Repository</name>
              <url>https://maven.atlassian.com/public</url>
          </repository>
          <snapshotRepository>
              <id>atlassian-public-snapshot</id>
              <name>Atlassian Public Snapshot Repository</name>
              <url>https://maven.atlassian.com/public-snapshot</url>
          </snapshotRepository>
      </distributionManagement>
      <developers>
            <developer>
                <id>jwesleysmith</id>
                <name>Jed Wesley-Smith</name>
                <email>jwesleysmith@atlassian.com</email>
                <organization>Atlassian</organization>
                <organizationUrl>http://www.atlassian.com</organizationUrl>
            </developer>
      </developers>)
    , pomIncludeRepository := { (repo: MavenRepository) => false } // no repositories in the pom
    , scalaVersion := Version.scala212
    , crossScalaVersions  := Seq(Version.scala212, Version.scala211)
    , ReleaseKeys.crossBuild := true
    , autoScalaLibrary := false
    , scalacOptions ++= scalacFlags 
    , javacOptions ++= Seq("-encoding", "UTF-8")
    , resolvers ++= Seq(
      Resolver.bintrayRepo("non", "maven"),
      "Scalaz Bintray Repo" at "https://dl.bintray.com/scalaz/releases"
    )
    , mappings in (Compile, packageBin) ++= Seq(
        file("LICENSE") -> "META-INF/LICENSE"
      , file("NOTICE")  -> "META-INF/NOTICE"
      )
    , credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
    , addCompilerPlugin("org.spire-math"        %% "kind-projector" % "0.9.3" cross CrossVersion.binary)
    , libraryDependencies <+= (scalaVersion)("org.scala-lang" % "scala-reflect" % _)
    )
}
