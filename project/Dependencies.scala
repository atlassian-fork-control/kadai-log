import sbt._

object Dependencies {

  object Version {
    val scala212       = "2.12.2"
    val scala211       = "2.11.11"
    val log4j          = "2.5"
    val specs2         = "3.8.7"
    val scalacheck     = "1.13.5"
    val junit          = "4.12"
    val kadai          = "6.0.2"
    val argonaut       = "6.2"
    val nscalaTime     = "2.16.0"
    val circe          = "0.7.0"
  }

  lazy val commonTest = 
    Seq(
      "org.specs2"          %% "specs2-core"        % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-junit"       % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-scalacheck"  % Version.specs2     % "test"
    , "org.specs2"          %% "specs2-mock"        % Version.specs2     % "test"
    , "org.scalacheck"      %% "scalacheck"         % Version.scalacheck % "test"
    , "junit"                % "junit"              % Version.junit      % "test"
    )

  lazy val kadai =
    Seq("io.atlassian"      %% "kadai-core"         % Version.kadai)
  
  lazy val log4j =
    Seq(
      "org.apache.logging.log4j"  % "log4j-api"     % Version.log4j
    , "org.apache.logging.log4j"  % "log4j-core"    % Version.log4j
    )

  lazy val argonaut =
    Seq(
      "io.argonaut"         %% "argonaut"             % Version.argonaut
    , "io.argonaut"         %% "argonaut-scalaz"      % Version.argonaut
    )

  lazy val circe =
    Seq(
      "io.circe"            %% "circe-core"         % Version.circe
    , "io.circe"            %% "circe-jawn"         % Version.circe      % Test
    )

  lazy val nscalaTime = 
    Seq("com.github.nscala-time"   %% "nscala-time" % Version.nscalaTime)
}
