package kadai.log
package log4j

import io.circe.{ Encoder, Json }
import io.circe.jawn.JawnParser
import io.circe.syntax._
import org.specs2.{ ScalaCheck, Specification }
import org.scalacheck.Prop
import org.apache.logging.log4j.Level
import org.specs2.runner.JUnitRunner
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class JsonLayoutSpec extends Specification with ScalaCheck with json.JsonLogging {
  import json.JsonLogging._

  def is = s2"""
  JsonLayout
    should work       $test
  """

  def test =
    Prop.forAll { (name: String, str: String) =>
      val foo = Foo(str)
      val parser = new JawnParser
      val value = new JsonLayout("nospaces", "UTF8").toSerializable(LogEvent(name, Log4jMessage(LogWriter[Foo].apply(foo))))
      parser.decode[Event](value) must beLike {
        case Right(Event(ts, n, level, context, msg, thread, thrown)) =>
          name === n and level === Level.INFO and context === None and msg === List("Foo" -> Foo.FooEncoder(foo))
      }
    }
}

case class Foo(s: String)
object Foo {
  implicit val FooEncoder: Encoder[Foo] = Encoder.instance {
    case Foo(s) =>
      Json.obj("Foo!" -> s.asJson, "Bar" -> "bar".asJson)
  }
}
